var app_id='Ca87AQAjuyqHdHtyag6fsNCmH';
var model={};
var device_token='';
var backbutton=0;
var set={};
var get_token='';

angular.module('starter', ['ionic', 'starter.controllers', 'starter.services','angularRipple','ngCordova'])
.run(function($ionicPlatform,$ionicPopup,$ionicHistory,loginServices,mylaipush,dashBoardOffers) {
  $ionicPlatform.ready(function() {

    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      if (ionic.Platform.isAndroid()) {
       StatusBar.backgroundColorByHexString("#006064");
     } else {
       StatusBar.styleLightContent();
     }
   }
   $ionicPlatform.registerBackButtonAction(function(e) {
     e.preventDefault();
     function showConfirm() {
      var confirmPopup = $ionicPopup.show({
       title : 'Exit Fund Collection?',
       template : 'Are you sure you want to exit Fund Collection?',
       buttons : [{
        text : 'Cancel',
        type : 'button-positive button-outline',
      }, {
        text : 'Ok',
        type : 'button-positive',
        onTap : function() {
          loginServices.logout();
          localStorage.removeItem("offers");
          ionic.Platform.exitApp();
        }
      }]
    });
    };
    if ($ionicHistory.backView()) {
      $ionicHistory.backView().go();
    } else {
      showConfirm();
    }
    return false;
  }, 101);


 dashBoardOffers.all().then(function(data){
  localStorage.setItem('offers',JSON.stringify(data));
});

  model=ionic.Platform.device();
      var push = PushNotification.init({"android":{ "senderID": "286234273489"}});
      push.on('registration', function(data) {
        device_token=data.registrationId;
        get_token=JSON.parse(localStorage.getItem('device_token'));
        if(get_token != device_token || !get_token || get_token=='' || get_token==null){
          localStorage.setItem('device_token',JSON.stringify(device_token));
          var obj={'app_id':app_id,'device_token':device_token,'model':model.model,'platform':model.platform,'status':true}
          mylaipush.notification(obj);
        }

      });
  /*push.on('notification', function(data) {
    var a=data.additionalData.openUrl.split("_");
    if(a[0]=="notification"){
      $state.go('details',{'obj':{'page':a[0],'category':a[1],'id':a[2]}});
    }
  });*/
 

  });


})


.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'    
  })
  .state('forget', {
    url: '/forget',
    cache:false,
    templateUrl: 'templates/forgot.html',
    controller: 'forgetCtrl'    
  })
  .state('user_profile', {
    url: '/user_profile',
    params:{obj:null},
    cache:false,
    templateUrl: 'templates/user_profile.html',
    controller: 'ProfileCtrl',
  })
  .state('agentProfile', {
    url: '/agentProfile',
    params:{obj:null},
    cache:false,   
    templateUrl: 'templates/agentProfile.html',
    controller: 'ProfileCtrl'    
  })
  .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })
  .state('tab.dashboard', {
    url: '/dashboard',
    views: {
      'tab-dashboard': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'dashboardCtrl'
      }
    }
  })
  .state('tab.req_customer', {
    url: '/dashboard/req_customer',
    cache:false,
    views: {
      'tab-dashboard': {
        templateUrl: 'templates/request_new_customer.html',
        controller: 'req_CustomerCtrl'
      }
    }
  })
  .state('tab.offers', {
    url: '/offers',
    cache:false,
    views: {
      'tab-offers': {
        templateUrl: 'templates/tab-offers.html',
        controller: 'OffersCtrl'
      }
    }
  })
  .state('tab.user_account', {
    url: '/user_account',
    cache:false,
    views: {
      'tab-account': {
        templateUrl: 'templates/user_account.html',
        controller: 'user_accountCtrl'
      }
    }
  })
  .state('tab.user_paymentmode', {
    url: '/user_account/:accoundId',
    cache:false,
    views: {
     'tab-account': {
      templateUrl: 'templates/user_payment_mode.html',
      controller: 'user_paymentmodeCtrl'
    }
  }
})
  .state('register', {
    url: '/register',
    views: {
      '': {
        templateUrl: 'templates/register.html',
        controller: 'registerCtrl'
      }
    }
  })
  .state('tab.transaction', {
    url: '/transaction',
    cache:false,
    views: {
      'tab-transaction': {
        templateUrl: 'templates/userTransaction.html',
        controller: 'userTransactionCtrl'
      }
    }
  })
  .state('tab.singleTransaction', {
   url: '/transaction/:TransactionId',
   cache:false,
   views: {
     'tab-transaction': {
       templateUrl: 'templates/singleTransaction.html',
       controller: 'TransactionDetailCtrl'
     }
   }
 })
  .state('agent', {
    url: '/agent',
    abstract: true,
    templateUrl: 'templates/agentTabs.html'
  })
  .state('agent.agentTransaction', {
    url: '/agentTransaction',
    views: {
      'agent-agentTransaction': {
        templateUrl: 'templates/agentTransaction.html',
        controller: 'AgentTransactionCtrl'
      }
    }
  })
  .state('agent.agentsingleTransaction', {
   url: '/agentTransaction/:TransactionId',
   cache:false,
   views: {
     'agent-agentTransaction': {
       templateUrl: 'templates/agentsingleTransaction.html',
       controller: 'AgentSingleTransactionDetailCtrl'
     }
   }
 })
  .state('agent.dash', {
    url: '/dash',
    views: {
      'agent-agent': {
        templateUrl: 'templates/agent_dash.html',
        controller: 'dashboardCtrl'
      }
    }

  })
  .state('agent.add_customer', {
    url: '/dash/add_customer',
    views: {
      'agent-agent': {
        templateUrl: 'templates/add_new_customer.html',
        controller: 'add_CustomerCtrl'
      }
    }    
  })
  .state('agent.agentAccount', {
    url: '/agentAccount',
    cache:false,
    views: {
      'agent-agentAccount': {
        templateUrl: 'templates/agentAccount.html',
        controller: 'agentAccountCtrl'
      }
    }
  })
  .state('agent.agent_paymentmode', {
    url: '/agentAccount/:accoundId',
    cache:false,      
    views: {
     'agent-agentAccount': {
      templateUrl: 'templates/agent_payment_mode.html',
      controller: 'agent_paymentmodeCtrl'
    }
  }
})
  .state('agent.agentOffers', {
    url: '/agentOffers',
    cache:false,
    views: {
      'agent-agentOffers': {
        templateUrl: 'templates/agentOffers.html',
        controller: 'AgentOffersCtrl'
      }
    }
  });
  $urlRouterProvider.otherwise('/login');
})
.directive('pwCheck', function () {
	return {
		require: 'ngModel',
		link: function (scope, elem, attrs, ctrl) {
			scope.$watch(attrs.pwCheck, function (confirmPassword) {
    			var isValid = ctrl.$viewValue === confirmPassword;
    			ctrl.$setValidity('pwmatch', isValid);
            });
		}
	}
});	
 // var url = 'http://localhost/fundcollection/';
 var url = 'http://fund.mylaporetoday.in/';

