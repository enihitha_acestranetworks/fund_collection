
angular.module('starter.controllers', [])
.controller('loginCtrl',function($scope,$http,$state,$ionicHistory,$ionicSideMenuDelegate,loginServices,$rootScope,mylaipush,sendSms,notification,$ionicLoading,dashBoardOffers){
  $ionicSideMenuDelegate.canDragContent(false);
  $scope.login = function(data){
    this.data = {};
    if(data.id != '' && data.password != '')
{
    loginServices.login(data).then(function(response){
      $rootScope.name = response.name;
      $rootScope.unique_id = response.unique_id;
      $rootScope.mobile = response.mobile;

      message = "Hello "+response.name+" welcome to fund collection app";
      title = 'welcome';
      if(response.push_notification_status == 1)
      {
        notification.login_notifcation(message,title);
      }

      sendSms.send_sms('+91'+$rootScope.mobile,message);



    })
}
else
{
  alert("fill all the field");
  }
}
})
.controller('forgetCtrl',function($scope,$http,$state,$ionicHistory,$ionicSideMenuDelegate,ForgetPasswordServices,sendSms){
  $ionicSideMenuDelegate.canDragContent(false);
  $scope.step1=true;$scope.step2=false;$scope.step3=false;
  mobile_number = '';
  unique_id = '';
  $scope.getotp = function(data){
    unique_id = data.id;
    ForgetPasswordServices.sendotp(data).then(function(response){
      mobile_number = response;
      this.data = {};
      otp = Math.floor(100000 + Math.random()*100000);
      localStorage.setItem("otp",otp);
      message = "OTP for reset password is:"+otp;
      sendSms.send_sms('+91'+mobile_number,message);
       $scope.step1=false;$scope.step2=true;$scope.step3=false;
    })
   $scope.resend_otp = function(){
      otp = Math.floor(100000 + Math.random()*100000);
     localStorage.setItem("otp",otp);
      message = "OTP for reset password is:"+otp;
      sendSms.send_sms('+91'+mobile_number,message);
   }
  }
  $scope.otp_verification = function(data){
    if(data.otp == localStorage.getItem("otp"))
    {
      $scope.step1=false;$scope.step2=false;$scope.step3=true; 
      this.data = {};
      localStorage.removeItem("otp",otp);
    }
    else
    {
      alert('otp does not match');
    }
  }
  $scope.change_password = function(data){
    if(data.password == data.re_password)
    {
      data.id = unique_id;
      this.data = {}; 
      $scope.message = ForgetPasswordServices.PasswordChange(data).then(function(data){
        $state.go('login');
      });

    }
    else
    {
      alert("password mismatched");
    }
  }
})
.controller('registerCtrl',function($scope,$http,$state,$ionicSideMenuDelegate,register){
  $ionicSideMenuDelegate.canDragContent(false)
  $scope.register = function(data){
   register.registration(data);
   this.data = {};
 }
})

.controller('dashboardCtrl',function(dashBoardOffers,$scope,$http){

  $scope.dashboardoffers = JSON.parse(localStorage.getItem('offers'));

})
.controller('user_accountCtrl', function($scope,$rootScope,$state,AgentAccount,$ionicPlatform,Customers,razorpay,RazorpaySuccessCallback) {
  $scope.name = $rootScope.name;
  var accounts =  {};
  AgentAccount.get($rootScope.unique_id).then(function(data){
   if(data.status=="success"){
  $scope.accounts = data.data;
   $scope.show = false;
  $scope.show_account = true;
}
else
{

 $scope.show = true;
  $scope.show_account = false;

  $scope.accounts_text = data.text;
}
   accounts = data.data;
 });
  var razorpayData = {};
  var called = false
  var successCallback = function(payment_id) {
    called = false
    razorpayData.unique_id = accounts.unique_id;
    razorpayData.scheme_id = accounts.customer_vs_scheme_id;
    razorpayData.agent_key = accounts.agent_key;
    razorpayData.amount =  accounts.amount;
    razorpayData.due_id =  accounts.due_id;
    razorpayData.payment_id =  payment_id;
    RazorpaySuccessCallback.successCallback(razorpayData).then(function(success_data){
     alert(success_data.text);
   })
  };
  var cancelCallback = function(error) {
    alert(error.description + ' (Error ' + error.code + ')');
    called = false
  };
  $ionicPlatform.ready(function(){
    $scope.pay = function(unique_id,booking_number,due_date,agent_key,amount) {
      razorpayData.unique_id = unique_id;
      razorpayData.booking_number = booking_number;
      razorpayData.agent_key = agent_key;
      razorpayData.amount =  amount;
      razorpay.get_details(unique_id,booking_number,due_date).then(function(razorpay_data){
        var options = {
          description: razorpay_data.booking_number,
          image: razorpay_data.image,
          currency: razorpay_data.currency,
          key: razorpay_data.key,
          amount: razorpay_data.total,
          name: razorpay_data.name,
          prefill: {
            email:razorpay_data.prefill.email ,
            contact:razorpay_data.prefill.mobile ,
            name:razorpay_data.prefill.name},
            theme:razorpay_data.theme
          };
          console.log(options);
          if (!called) {
            RazorpayCheckout.open(options, successCallback, cancelCallback);
            called = true
          }
        })
    }
  });
})
.controller('agentAccountCtrl', function($scope,Customers,AgentAccount,$state,$rootScope) {
  $scope.Customer=false;
  $rootScope.cus_mobile = '';
  $scope.search = function() {
    unique_id = '';
    $scope.Customerlist=true;
    Customers.agent_customer($rootScope.unique_id).then(function(data){
     $scope.customers = data;
   });
  }                        
  $scope.cus_list= function(id,img,name,mobile)
  { 
    $rootScope.cus_mobile = mobile;  
    $scope.customer_id=id;
    AgentAccount.get(id).then(function(data){
     $scope.img_name = img;
     $scope.cusname = name;
     if(data.status == "success")
     {
     $scope.accounts = data.data;
   }
   else
   {
    $scope.accounts = '';
    alert(data.text);
   }
     $scope.Customer=true;
     $scope.Customerlist=false;
   });
  } 
})

.controller('agent_paymentmodeCtrl', function($scope,$stateParams,$rootScope,CustomerAccount,$state,ChequeDetailsEntry,CashDetailsEntry,sendSms,razorpay,RazorpaySuccessCallback) {
  $scope.step1=true; $scope.step2=false;$scope.step3=false;$scope.step4=false;
  CustomerAccount.get($stateParams.accoundId).then(function(data){
    if(data.status == "success")
    {
   $scope.accounts = data.data;
 }
 })
  mode ='';
  unique_id =  '';
  customer_vs_scheme_id = '';
  agent_key = '';
  amount = '';
  $scope.send_otp= function(data)
  { 
    mode =data;
    otp = Math.floor(100000 + Math.random()*100000);
      localStorage.setItem("otp",otp);
      message = "OTP for payment_process is:"+otp;
      sendSms.send_sms('+91'+ $rootScope.mobile,message);
    $scope.step1=false; $scope.step2=true;$scope.step3=false;$scope.step4=false;  
  }
  $scope.payment_process= function(accounts,mode,otp)
  {   
    unique_id = accounts.unique_id;
    customer_vs_scheme_id = accounts.customer_vs_scheme_id;
    agent_key = accounts.agent_key;
    amount =  accounts.total;
    due_id = accounts.id;
    $scope.amount= amount;
    if(otp == 1234)
    {
      if('Cheque'==mode)
      {
        $scope.step1=false; $scope.step2=false;$scope.step3=true;$scope.step4=false;
      }
      else if('Cash'==mode)
      {
        $scope.step1=false; $scope.step2=false;$scope.step3=false;$scope.step4=true;
      }
      else
      {
        $scope.step1=false; $scope.step2=false;$scope.step3=false;$scope.step4=false;
        razorpay.get_details(unique_id).then(function(razorpay_data){
         var called = false
         var successCallback = function(payment_id) {
          called = false
          razorpayData.unique_id = unique_id;
          razorpayData.scheme_id = customer_vs_scheme_id;
          razorpayData.agent_key = agent_key;
          razorpayData.amount =  amount;
          razorpayData.due_id =  due_id;
          razorpayData.payment_id =  payment_id;
          RazorpaySuccessCallback.successCallback(razorpayData).then(function(success_data){
           alert(success_data.text);
         })
        };
        var cancelCallback = function(error) {
          alert(error.description + ' (Error ' + error.code + ')');
          called = false
        };
        var options = {
          description: razorpay_data.scheme_name,
          image: razorpay_data.image,
          currency: razorpay_data.currency,
          key: razorpay_data.key,
          amount: razorpay_data.total,
          name: razorpay_data.name,
          prefill: {
            email:razorpay_data.prefill.email ,
            contact:razorpay_data.prefill.mobile ,
            name:razorpay_data.prefill.name},
            theme:razorpay_data.theme
          };
          if (!called) {
            RazorpayCheckout.open(options, successCallback, cancelCallback);
            called = true
      $state.go('agent.dash');
            
          }
        })
      }  
    }
    else
    {
      alert("enter correct otp");
    }
  }
  $scope.cheque_submit= function(data)
  { 
    data.unique_id = unique_id;
    data.customer_vs_scheme_id = customer_vs_scheme_id;
    data.mode = mode;
    data.agent_key = agent_key;
    data.amount =  amount;
    data.due_id = due_id;
     this.data = {};
    ChequeDetailsEntry.Cheque(data).then(function(output){
     message = "Your due amount "+amount+" is payed in the mode of Cheque successfully. The Cheque number is "+output;
     alert(message);
      sendSms.send_sms('+91'+ $rootScope.cus_mobile,message);
      $state.go('agent.dash');
   })
  }
  $scope.cash_submit= function(data)
  {
    data.unique_id = unique_id;
    data.customer_vs_scheme_id = customer_vs_scheme_id;
    data.mode = mode;
    data.agent_key = agent_key;
    data.due_id = due_id;
    data.amount =  amount;
     this.data = {};
    $scope.message = CashDetailsEntry.Cash(data).then(function(response_data){
      message = "Your due amount"+amount+" payed in the mode of cash successfully";
     alert(message);
      sendSms.send_sms('+91'+$rootScope.cus_mobile,message);
      $state.go('agent.dash');

   })
  }
})
.controller('AgentOffersCtrl', function($scope, Offers,Customers,$state,$rootScope) {
  $scope.Customer=false;
  $scope.search = function() {
   $scope.Customerlist=true; 
   Customers.agent_customer($rootScope.unique_id).then(function(data){
     $scope.customers = data;
   });
 }
 $scope.show_offer= function(id,img,name)
 { 
  Offers.get(id).then(function(data){
    if(data.status == "success")
    {
    $scope.offers = data.data;
  }
  else
  {
    $scope.offers = '';
    alert(data.text);
  }
  })
  $scope.img_name = img;
  $scope.customer_id=id;
  $scope.cusname = name;
  $scope.Customer=true;
  $scope.Customerlist=false; 
}
})
.controller('OffersCtrl', function($scope, Offers,$rootScope) {
  
 Offers.get($rootScope.unique_id).then(function(data){
  if(data.status=="success"){
  $scope.offers = data.data;
  $scope.show = false;
}
else
{
  $scope.show = true;
  $scope.offer_text = data.text;
}
})
 $scope.customer_id=$rootScope.unique_id;
 $scope.customers = "";

})
.controller('ProfileCtrl', function($scope, $state,$ionicActionSheet,$stateParams,$timeout,ProfileServices,$ionicHistory) {
 $scope.profileData = $stateParams.obj;
 $scope.form1=true;$scope.form2=false;$scope.form3=false;
 $scope.editProfile = function() {
   var hideSheet = $ionicActionSheet.show({
     buttons: [
     { text: 'Edit Profile' },
     { text: 'Change Password' }

     ],
     titleText: 'Modify your Profile',
     cancelText: 'Cancel',
     cancel: function() {
      return true;
    },
    buttonClicked: function(index) {
      if ('0'== index) 
      {
        $scope.form1=false;$scope.form2=true;$scope.form3=false;return true;
      }
      else if('1'== index)
      {
        $scope.form1=false;$scope.form2=false;$scope.form3=true;return true;
      }
      else if('2'== index)
      {
        $scope.form1=false;$scope.form2=false;$scope.form3=false;return true;
      }
      else{
        return true;
      }    
    }
  });
   $timeout(function() {
     hideSheet();
   }, 5000);
 }
 $scope.edit_profile= function(data)
 {
   data.unique_id = $scope.profileData.unique_id;
   ProfileServices.Edit(data).then(function(value){
    $scope.message = value;
    $scope.form1=true;$scope.form2=false;$scope.form3=false;
    $scope.profileData.name = data.User_name;
  });
 }
 $scope.change_password= function(data)
 {
   console.log(data);
   ProfileServices.PasswordChange(data).then(function(response){
    $scope.message = response;
    $scope.form1=true;$scope.form2=false;$scope.form3=false;
  });
 }  
 $scope.goBack = function() {
  $ionicHistory.goBack();
}
})
.controller('add_CustomerCtrl', function($scope,$rootScope,$stateParams, $state, addCustomerServices) {
  $scope.step1=true; $scope.step2=false;
  $scope.accounts =$stateParams.add_customer;
  $scope.savestep1= function()
  {
    $scope.step1=false; $scope.step2=true;
    $scope.names = ["Directory", "Classifieds", "Advertisement"];
    $scope.types = ["Type1", "Type2", "Type3"];
  }
  $scope.add_customer= function(data,Category,Type)
  { 
    data.unique_id =$rootScope.unique_id;
    addCustomerServices.addCustomer(data,Category,Type).then(function(response){
     $scope.message = response;
   });
  }
})

.controller('req_CustomerCtrl', function($scope, $stateParams,$state,addCustomerServices,$rootScope) {
  $scope.accounts = $stateParams.req_customer; 
  $scope.req_customer= function(data)
  {   
    data.unique_id = $rootScope.unique_id;
    addCustomerServices.reqNewCustomer(data).then(function(response_data){
     $scope.message = response_data;
     if(response_data.status == "success")
     $state.go('tab.dashboard');
   })
    this.data = {};
  }
})
.controller('AgentTransactionCtrl', function($scope,Customers,Transaction,$state,$rootScope) {
  $scope.Customer=false;
  unique_id = $rootScope.unique_id;	
  $scope.search = function() {
    $scope.Customerlist=true;
    Customers.agent_customer(unique_id).then(function(data){
      $scope.customers = data;
    })
  }
  $scope.show_transaction= function(id,img,name)
  { 
    Transaction.get(id).then(function(data){
      if(data.status == "success")
      {
      $scope.transaction = data.data;
    }
    else
    {
      $scope.transaction = '';
      alert(data.text);
    }
    });
    $scope.img_name = img;
    $scope.customer_id=id;
    $scope.cusname = name;
    $scope.Customer=true;
    $scope.Customerlist=false; 
  } 
})

.controller('AgentSingleTransactionDetailCtrl', function($scope, $stateParams,$ionicHistory,Transaction) {
  var data = $stateParams.TransactionId;
  Transaction.singleTransaction(data).then(function(data){
    if(data.status == "success")
    {
    $scope.singleTransaction = data.data;
  }
    $scope.goBack = function(){
     $ionicHistory.goBack();
   } 
 });
})
.controller('userTransactionCtrl',function($scope,Transaction,$rootScope){
 Transaction.get($rootScope.unique_id).then(function(data){
  if(data.status=="success"){
  $scope.transaction = data.data;
  $scope.show = false;
}
else
{
  $scope.show = true;
  $scope.transaction_text = data.text;
}
});
})
.controller('TransactionDetailCtrl', function($scope, $stateParams,$ionicHistory,Transaction) {
  var data = $stateParams.TransactionId;
  Transaction.singleTransaction(data).then(function(data){
     if(data.status == "success")
    {
    $scope.singleTransaction = data.data;
  }
    $scope.goBack = function(){
     $ionicHistory.goBack();
   } 
 });
})

.controller('profileCtrl', function($scope,$state,loginServices,$ionicSideMenuDelegate,$rootScope,loginServices,sendSms) {
  $scope.getProfile = function(){
   $scope.profileData= loginServices.getProfile();
   if($scope.profileData.role == 'agent')
   {
    $state.go('agentProfile',{obj:$scope.profileData});
    $ionicSideMenuDelegate.canDragContent(true)
  }
  else if($scope.profileData.role == 'user')
  {
   $state.go('user_profile',{obj:$scope.profileData});
   $ionicSideMenuDelegate.canDragContent(true)
 }
}
$rootScope.signout = function(){
  $ionicSideMenuDelegate.canDragContent(false)
  loginServices.logout().then(function(response){
    if(response.status == "success"){
    message = "You are successfully logged out";
    sendSms.send_sms('+91'+$rootScope.mobile,message);
  }
  })
}
});
