angular.module('starter.services', [])
.factory('dashBoardOffers', function( $http) {
  return {
    all:function(){
      data = {};
      data.action = 'dashBoardOffers';
      return $http.post(url,data).then(function(response){
       if(response.data.status == "error")
       {
         alert(response.data.text);
       }
       else{
        return response.data.data;
      }
    },function(error){
    })
    }
  }
})
.factory('Customers', function($http) {
  customer_list = '';
  return {
    agent_customer:function(unique_id){
     data = {};
     data.action = 'list_customer';
     data.unique_id = unique_id;
     return $http.post(url,data).then(function(response){
       if(response.data.status == "error")
       {
         alert(response.data.text);
       }
       else{
        return response.data.data;
      }
    },function(error){
    })
   }
 }
})

.factory('Offers', function($http) {
 return {
  get:function(id){
    data = {};
    data.action = 'offers';
    data.id = id;
    return $http.post(url,data).then(function(response){
    return response.data;
  },function(error){
  })
  } 
}
})

.factory('Transaction', function($http, $state) {
 return {
  get:function(id){
    data = {};
    data.action = 'transaction';
    data.id = id;
    return $http.post(url,data).then(function(response){
    return response.data;
  },function(error){
    console.log(error);
  })
  },
  singleTransaction :function(id){
   data = {};
   data.action = 'singleTransaction';
   data.id = id;
   return $http.post(url,data).then(function(response){
     return response.data;
   },function(error){
     console.log(error);
   })
 }
}
})

.factory('AgentAccount', function($http) {
  return {
    get:function(id){
      ac_data = {};
      ac_data.token = 123456789;
      ac_data.unique_id = id;
      ac_url = 'http://acevilambaram.mylaporetoday.in/api/chit_scheme';
     return  $http.post(ac_url,ac_data).then(function(response){
       if(response.data.status == "success")
       {
        angular.forEach(response.data.data,function(value,key){

       data = {};
      data.action = 'account_due';
      data.unique_id = id;
      data.booking_number = value.booking_number;
      data.billing_date = value.billdate;
      data.bill_amount = value.bill_amount;
      data.charges = value.charges;
      data.total = data.bill_amount+data.charges;
      data.due_date = value.duedate;
        })
      return $http.post(url,data).then(function(response){
       return response.data;
    },function(error){
      console.log(error);
    })
    }
    else
    {
      console.log(response.data);
    }
      },function(error){
      console.log(error);
    })
    }
  }
})

.factory('CustomerAccount', function($http) {
  return {
    get:function(id){
      data = {};
      data.action = 'single_due';
      data.id = id;
      return $http.post(url,data).then(function(response){
    return response.data;
    },function(error){
      console.log(error);
    })
    }
  }
})

.factory('ChequeDetailsEntry', function($http) {
 return {
  Cheque: function(data){
    data.action = 'cheque';
    return $http.post(url,data).then(function(response){
     if(response.data.status == "error")
     {
      alert(response.data.text);
    }
    else{
      return response.data.data;
    }
  },function(error){
   console.log(error);
 })
  }
}
})

.factory('CashDetailsEntry', function($http) {
  return {
    Cash: function(data) {
      data.action =  'cash';
      return $http.post(url,data).then(function(response){
       if(response.data.status == "error")
       {
         alert(response.data.text);
       }
       else{
        return response.data.text;
      }
    },function(error){
      console.log(error);
    })
    }
  };
})

.factory('ForgetPasswordServices', function($http) {
  return {
    sendotp: function(data) {
      data.action =  'get_mobile_number';
      console.log(data);
      return $http.post(url,data).then(function(response){
       if(response.data.status == "error")
       {
         alert(response.data.text);
       }
       else{
        return response.data.data;
      }
    },function(error){
     console.log(error);
   })
    },
    PasswordChange: function(data) {
     data.action =  'forgetPassword';
     return $http.post(url,data).then(function(response){
       if(response.data.status == "error")
       {
         alert(response.data.text);
       }
       else{
        return response.data.text;
      }
    },function(error){
      console.log(error);
    })
   }
 };
})

.factory('ProfileServices', function($http) {
  return {
    Edit: function(data) 
    {
      data.action = 'editProfile';
      return $http.post(url,data).then(function(response){
        if(response.data.status == "error")
        {
          alert(response.data.text);
        }
        else{
          return response.data.text;
        }
      },function(error){
        console.log(error);
      })
    },
    PasswordChange: function(data) {
     data.action = 'changePassword';
     if(data.new_password && data.re_enter_new_password){
      return $http.post(url,data).then(function(response){
        if(response.data.status == "error")
        {
          alert(response.data.text);
        }
        else{
          return response.data.text;
        }
      },function(error){
        console.log(error);
      })
    }
    else{
      alert("password does not match");
    }
  }
};
})

.factory('addCustomerServices', function( $http) {
 return {
  addCustomer: function(data,Category,Type) {
    data.action = 'create_customer';
    return $http.post(url,data).then(function(response){
      if(response.data.status == "error")
      {
        alert(response.data.text);
      }
      else{
        return response.data.data;
      }
    },function(error){
      console.log(error);
    })
    var msg='addCustomer Successfully';
    return msg;
  },
  reqNewCustomer: function(data) {
    data.action = 'customerSuggest';
    return $http.post(url,data).then(function(response){
      if(response.data.status == "error")
      {
        alert(response.data.text);
      }
      else{
        return  response.data;
      }
    },function(error){
      console.log(error);
    })
  }
};
})
.factory('loginServices', function($http,$ionicHistory,$ionicSideMenuDelegate,$state,$rootScope) {
  var profiledata = "";
  return {
    login: function(data) {

      data.device_id = Math.floor(Math.random()*10000);
      // data.device_id = device_token;


      data.action = 'login';
      return $http.post(url,data).then(function(response){
        if(response.data.status == "error")
        {
          alert(response.data.text);
        }
        else
        {
          localStorage.setItem("token",JSON.stringify(response.data.data.token));
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          profiledata=response.data.data;
          $rootScope.profile_img=profiledata.image_name;
          $rootScope.unique_id = profiledata.unique_id;
          if(response.data.data.role == 'agent')
          {
            $state.go('agent.dash');
            $ionicSideMenuDelegate.canDragContent(true)
          }
          else if(response.data.data.role == 'user')
          {
           $state.go('tab.dashboard');
           $ionicSideMenuDelegate.canDragContent(true)
         }
         return response.data.data;
       }
     },
     function error(response)
     {
      alert("login failed"+response.statusText);
    });
    },
    getProfile: function() {
      return profiledata;
    },
    logout :function(){
      data  =  {};
      data.token = localStorage.getItem("token");
      data.action = 'logout';
      data.device_id = '12345';
      data.unique_id = $rootScope.unique_id;
      return $http.post(url,data).then(function(response){
        if(response.data.status == "error")
        {
          alert(response.data.text);
        }
        else
        {
          localStorage.removeItem("token");
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          $state.go('login');
          return response.data; 
        }
      },
      function(error){
        console.log(error);
      })
    }
  };
})

.factory('razorpay',function($http){
  return{
   get_details: function(id,booking_number,due_date){
     data = {};
     data.action = 'Razorpay';
     data.id = id;
     data.booking_number = booking_number;
     data.due_date = due_date;
     return $http.post(url,data).then(function(response){
      if(response.data.status == "error")
      {
        alert(response.data.text);
      }
      else{
        return response.data.data;
      }
    },function(error){
      console.log(error);
    })
   }
 }
})

.factory('RazorpaySuccessCallback',function($http){
 return{
   successCallback: function(razorpayData){
    razorpayData.payment_id =  123456789;
    razorpayData.action = 'RazorpaysuccessCallback';
    return $http.post(url,razorpayData).then(function(response){
      if(response.data.status == "error")
      {
        alert(response.data.text);
      }
      else{
        return response.data;
      }
    },function(error){
      console.log(error);
    })
  }
}
})

.factory('register',function($http,$state){
  new_data =  {};
 return{
  registration: function(data){
    console.log(data);
    new_data.token = 123456789;
    new_data.unique_id = data.id;
    reg_url = 'http://acevilambaram.mylaporetoday.in/api/chit_registration';
    $http.post(reg_url,new_data).then(function(response){
       if(response.data.status == "error")
      {
        alert(response.data.text);
        $state.go('register');
      }
      else
      { 
    data.action = "registration";
        data.name = response.data.data.customer_name;
        data.id = response.data.data.unique_id;
        data.mobile = response.data.data.mobile_no;
        data.email = response.data.data.email_id;
        data.city = response.data.data.city;
        data.country = response.data.data.country;
        data.role = 'user';
        data.device_id = device_token;
        if(!response.data.image_name)
        {
          data.image_name = 'customer.jpg';
        }
        else
        {
          data.image_name = response.data.image_name;
        }
        console.log(data);
         $http.post(url,data).then(function(response){
      if(response.data.status == "error")
      {
        alert(response.data.text);
        $state.go('register');
      }
      else
      {
        localStorage.setItem("token",JSON.stringify(response.data.data.token));
        $state.go('login');
      }
    },function(error){
      console.log(error);
    })
       }
       },function(error){
      console.log(error);
    })
  }
}
})
.factory('sendSms', function($http) {
  return {
    send_sms: function(to,message) {
      data = {};
      data.to = to;
      data.message = message;
      data.action = 'send_sms';
      $http.post(url, data).then(function(result) {
      }, function(error) {
       console.log(error);
     });
    },
  };
})
.factory('mylaipush', function($http) {
  return {
    notification: function(obj) {
      $http.post('http://push.mylaporetoday.in/device_register', obj).then(function(result) {
        if (result.status == "success") {
        } else if (result.status == "error") {
          console.log(result.message);
        }
      }, function(error) {
        if (error == 500 || error == 404 || error == 0) {
          console.log("Push Api" + error);
        }
      });
    },
  };
})
.factory('notification',function($http){
  return{
    login_notifcation:function(message,title){
      data = {
      'device_token':JSON.parse(localStorage.getItem('device_token')),
      'message' :message,
      'title':title,
      'senderId':'Ca87AQAjuyqHdHtyag6fsNCmH',
      // ’openUrl’:openurl,
      // ’additionaparam1’:aditionalparam1, 
      // ’additionaparam2’:aditionalparam2 
    }
    $http.post( 'http://push.mylaporetoday.in/SendNotificationToMultidevice',data).then(function(response){
      console.log(response);
    });
  }
}
});
